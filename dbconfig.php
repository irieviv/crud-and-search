<?php
	/**
	*	Database connection file
	*/
	mb_internal_encoding('UTF-8');
	
	$host = 'localhost';
	$user = 'root';
	$pass = '';
	$db = 'crud_demo';
	
	$conn = mysqli_connect($host, $user, $pass, $db);
	if($conn)
	{
		mysqli_set_charset($conn, 'utf8');
	}
	else
	{
		echo 'Connection error!';
	}
?>