<?php
	include_once 'dbconfig.php';
	
	if(!empty($_POST))
	{
		//variables for input data
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$user_city = $_POST['user_city'];
		
		//sql query for inserting data into database
		$sql_query = "INSERT INTO users(first_name, last_name, user_city) VALUES ('$first_name', '$last_name', '$user_city')";
		$insert = mysqli_query($conn, $sql_query);
		if($insert)
		{
			header('Location: index.php');
			exit;
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Add data</title>
	</head>
	<body>
		<form method="post">
			<table>
				<tr>
					<th><a href="index.php">back to main page</a></th>
				</tr>
				<tr>
					<td>
						<input type="text" name="first_name" placeholder="First name" required="required" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="last_name" placeholder="Last name" required="required" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="user_city" placeholder="City name" required="required" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Submit" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>