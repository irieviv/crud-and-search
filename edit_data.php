<?php
	include_once 'dbconfig.php';
	
	if(isset($_GET['edit_id']))
	{
		$sql_query = 'SELECT * FROM users WHERE user_id=' . $_GET['edit_id'];
		$select_id = mysqli_query($conn, $sql_query);
		$fetched_row = mysqli_fetch_array($select_id);
	}
	
	if(!empty($_POST))
	{
		//variables for input data
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$user_city = $_POST['user_city'];
		
		//sql query for updating data into database
		$sql_query = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', user_city = '$user_city' WHERE user_id = " . $_GET['edit_id'];
		$update = mysqli_query($conn, $sql_query);
		if($update)
		{
			header('Location: index.php');
			exit;
		}
		else
		{
			$p_output = '<p>Update error</p>';
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Edit</title>
	</head>
	<body>
		<form method="post">
			<table>
				<tr>
					<td>
						<input type="text" name="first_name" placeholder="First name" value="<?php echo $fetched_row['first_name']; ?>" required="required" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="last_name" placeholder="Last name" value="<?php echo $fetched_row['last_name']; ?>" required="required" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="user_city" placeholder="City" value="<?php echo $fetched_row['user_city']; ?>" required="required" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Update" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>