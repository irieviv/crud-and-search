<?php
	include_once 'dbconfig.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Index</title>
	</head>
	<body>
		<form action="delete.php" method="get">
			<table>
				<tr>
					<th colspan="5">
						<a href="add_data.php">add data here.</a>
					</th>
				</tr>
				<tr>
					<th>First name</th>
					<th>Last name</th>
					<th>City name</th>
					<th colspan="2">Operations</th>
				</tr>
				<?php
					if(!empty($_GET['search_string']))
					{
						$search_string = $_GET['search_string'];
						$sql_query = "SELECT * FROM users WHERE first_name LIKE '%" . $search_string . "%'";
						$search = mysqli_query($conn, $sql_query);
					}
					
					if($search)
					{
						while($row = mysqli_fetch_row($search))
						{
							echo '<tr>
									<td>' . $row[1] . '</td>
									<td>' . $row[2] . '</td>
									<td>' . $row[3] . '</td>
									<td>
										<a href="edit_data.php?edit_id=' . $row[0] . '">Edit</a>
									</td>
									<td>
										<input type="checkbox" name="delete_id[]" value="' . $row[0] . '" />
									</td>
								</tr>';
						}
					}
					else
					{
						echo '<tr><th colspan="5">Selection error!</th></tr>';
					}
				?>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th><input type="submit" value="Delete" /></th>
				</tr>
			</table>
		</form>
		<br />
		<a href="index.php">Return to index</a>
	</body>
</html>